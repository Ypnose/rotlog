#!/bin/sh
# Script by Ypnose - http://ywstd.fr
set -e

# This script should be called using /etc/crontab or any other
# cron-like mechanism
# m  h  dom mon dow user  command
# 05 00 *   *   *   root  rotlog -s5 /var/log/messages

# Size in mebibytes and without the "extension"
RLOG_SIZE="1"
# String to append between filename and .gz for rotated files
RLOG_PATTERN="$(date '+%Y%m%d')"
# File ownership
RLOG_UID="root"
RLOG_GID="adm"
# Move rotated file to a specific directory
#RLOG_MOVETODIR=""
# Number of files to keep, 0 to disable
RLOG_KEEP="7"

usage() {
	printf "%s\n" "usage: ${0##*/} [-h] [-g FILE GROUP] [-k NUMBER OF FILES] [-s MAX FILE SIZE] [-u FILE OWNER] [FILE]"
	exit "${1:-0}"
}

p_err() {
	printf "%s\n" "$1" >&2
	exit 1
}

logger_syslog() {
	if [ -x "$(command -v logger)" ]; then
		logger -t "${0##*/}" -- "$1"
	fi
}

while getopts ":g:k:s:u:" opt; do
	case $opt in
		g)
			RLOG_GID="$OPTARG"
			;;
		k)
			RLOG_KEEP="$OPTARG"
			;;
		s)
			RLOG_SIZE="$OPTARG"
			;;
		u)
			RLOG_UID="$OPTARG"
			;;
		*)
			usage 1
			;;
	esac
done
shift $(( OPTIND - 1 ))

if [ "$1" = "-h" ] || [ -z "$1" ]; then
	usage
fi

RLOG_FILE="$1"
RLOG_BYTE="$(( RLOG_SIZE * 1024 ))"
RLOG_ROTATE="${RLOG_FILE}.${RLOG_PATTERN}.gz"

if [ ! -f "$RLOG_FILE" ]; then
	p_err "$RLOG_FILE not found"
fi

if [ "$(du -- "$RLOG_FILE" | cut -f1)" -ge "$RLOG_BYTE" ]; then
	if [ ! -e "$RLOG_ROTATE" ]; then
		gzip -9 -c -- "$RLOG_FILE" >"$RLOG_ROTATE"
		>"$RLOG_FILE"
		chmod 640 -- "$RLOG_FILE"
		chmod 440 -- "$RLOG_ROTATE"
		chown "${RLOG_UID}:${RLOG_GID}" -- "$RLOG_FILE" "$RLOG_ROTATE"
		pkill -HUP rsyslogd
		if [ -n "$RLOG_MOVETODIR" ]; then
			mkdir -p "$RLOG_MOVETODIR"
			mv "$RLOG_ROTATE" "$RLOG_MOVETODIR"
		fi
		logger_syslog "$RLOG_FILE was rotated"
	else
		p_err "$RLOG_FILE was already rotated to $RLOG_ROTATE"
	fi
fi

# Minimalistic counter without tail(1) for rotation
if [ "$RLOG_KEEP" -ne 0 ]; then
	n=1
	# Let's play very safe here
	if [ -n "$RLOG_MOVETODIR" ]; then
		[ ! -d "$RLOG_MOVETODIR" ] && p_err "$RLOG_MOVETODIR is missing"
		cd "$RLOG_MOVETODIR"
	fi
	find "${RLOG_FILE%/*}" -maxdepth 1 -type f -iname "${RLOG_FILE##*/}.*.gz" \
		| sort -r | while IFS=$'\n' read -r RLOG_GZ; do
			if [ "$n" -gt "$RLOG_KEEP" ]; then
				rm -- "$RLOG_GZ"
				logger_syslog "$RLOG_GZ was deleted (keeping $RLOG_KEEP files)"
			else
				n="$(( n + 1 ))"
			fi
	done
fi

exit
