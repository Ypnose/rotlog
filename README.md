rotlog - [rot][log] aka let your logs rot in hell
=================================================

A tiny shell script to rotate your logs.

## Why?

When dealing with a lot of logs (captured by ```rsyslogd(8)```), I came
to the conclusion that ```logrotate(8)``` is not enough reliable.

## Usage

	$ rotlog -h
	$ rotlog -s10 /var/log/syslog

For the possible options, read the sources.

This script was written to be used in combination with ```crond(8)```:

	$ cat /etc/crontab
	# m h dom mon dow user  command
	10 00   * * *   root  /my/strange/path/rotlog -s10 /var/log/kern.log

It is possible to edit variables directly within the script to modify
globally how ```rotlog``` behaves. Or you can also use command line
options to apply specific settings for one or more file(s).

[systemd](https://systemd.io/) service and timer units are included
in ```systemd/``` directory. For more information about timer unit,
read this
[page](https://www.freedesktop.org/software/systemd/man/systemd.timer.html).

## Author

Ypnose - http://ywstd.fr/

## License

BSD 3-Clause License. Check LICENSE.
